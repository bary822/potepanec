require 'rails_helper'

RSpec.feature 'User views category page', type: :feature do
  let(:taxon) { create :taxon, name: 'Python' }
  let!(:product) { create :product_with_option_types, taxons: [taxon] }

  let(:another_taxon) { create :taxon, name: 'Go-lang' }
  let!(:another_product) { create :product_with_option_types, taxons: [another_taxon] }

  scenario "User views category page" do
    visit potepan_category_url(taxon_id: taxon.id)

    expect(page).to have_link product.name
    expect(page).not_to have_link another_product.name

    expect(page).to have_content taxon.name.upcase
    expect(page).not_to have_content another_taxon.name.upcase

    click_link product.name
    uri = URI.parse(current_url)
    expect(uri.path).to eq "/potepan/products"
    expect(uri.query).to eq "id=#{product.id}"
  end
end

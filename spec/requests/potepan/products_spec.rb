require 'rails_helper'

RSpec.describe 'Potepan::Products', type: :request do
  describe 'GET /potepan/products?id=:id' do
    let(:product) { create :product_with_option_types }

    before do
      get potepan_products_url, params: { id: product.id }
    end

    it 'responds successfully' do
      expect(response).to be_successful
    end

    it 'returns product with given id' do
      expect(response.body).to include product.name
    end
  end
end

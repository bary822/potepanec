require 'rails_helper'

RSpec.describe 'Potepan::Categories', type: :request do
  describe 'GET /potepan/categories/:taxon_id' do
    let(:taxon) { create :taxon, name: 'Python' }
    let!(:product) { create :product_with_option_types, taxons: [taxon] }

    let(:another_taxon) { create :taxon, name: 'Go-lang' }
    let!(:another_product) { create :product_with_option_types, taxons: [another_taxon] }

    before do
      get potepan_category_url(taxon_id: taxon.id)
    end

    it 'responds successfully' do
      expect(response).to be_successful
    end

    it 'returns products tied to given taxon_id' do
      expect(response.body).to include product.name
      expect(response.body).to include taxon.name
    end

    it 'does not return products not tied to given taxon_id' do
      expect(response.body).not_to include another_product.name
      expect(response.body).not_to include another_taxon.name
    end
  end
end

class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:taxon_id])
    @products = @taxon.products.includes(master: :images)
    @taxonomies = Spree::Taxonomy.includes(taxons: :products)
  end
end
